import React from "react";
import MainLayout from "../../layout/Main";
import {inject, observer} from "mobx-react";
import {Grid, Row} from "react-flexbox-grid";

@inject("store")
@observer
class MainPage extends React.Component {

    updateDimensions = () => {
        this.props.store.setContainer(this.refs.main.offsetWidth);
    };

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    render() {

        const {camWidth, camHeight, grid} = this.props.store;

        return (
            <MainLayout>
                <div className="main_page" ref="main">
                    {Array.from(Array(grid[1]).keys()).map(key => {
                        return <Grid key={key}>
                            <Row>
                                {Array.from(Array(grid[0]).keys()).map(key => {
                                    return <div key={`cam${key}`} className="main_page__cams_block" style={{
                                        width: camWidth,
                                        height: camHeight
                                    }}>
                                        <div className="main_page__cams_block_wrapper">
                                            <img src={require("../../images/1481538845126476530.jpg")} alt="cam screen"/>
                                        </div>
                                    </div>
                                })}
                            </Row>
                        </Grid>
                    })}

                </div>
            </MainLayout>
        );
    }
}

export default MainPage;