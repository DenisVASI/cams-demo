import React from "react";
import {Grid, Row, Col} from "react-flexbox-grid";
import {Select} from 'antd';
import {inject, observer} from "mobx-react";

const {Option} = Select;

@inject("store")
@observer
class MainLayout extends React.Component {

    handleChange(value) {
        this.props.store.setGrid(JSON.parse(value));
    }

    render() {

        const {children} = this.props;

        return (
            <div className="main_layout">
                <nav className="main_layout__navigation">
                    <Grid>
                        <Row>
                            <Col xs={6}>
                                <div className="main_layout__logo">
                                    Cams Demo
                                </div>
                            </Col>
                            <Col xs={6}>
                                <div className="main_layout__select_grid">
                                    <Select defaultValue={"3 X 4"} style={{width: 120}}
                                            onChange={this.handleChange.bind(this)}>
                                        <Option value={JSON.stringify([3, 4])}>3 X 4</Option>
                                        <Option value={JSON.stringify([6, 4])}>6 X 4</Option>
                                        <Option value={JSON.stringify([12, 4])}>12 X 4</Option>
                                    </Select>
                                </div>
                            </Col>
                        </Row>
                    </Grid>
                </nav>

                <div className="main_layout__content">
                    <Grid>
                        {children}
                    </Grid>
                </div>
            </div>
        );
    }
}

export default MainLayout;