//libraries
import React from 'react';
import Store from "./store";
import {observer, Provider} from "mobx-react";
import {Router, Switch, Route} from "react-router";
import {createBrowserHistory} from "history";
//styles
import 'antd/dist/antd.css';
import './styles/App.sass';

//pages
import MainPage from "./pages/Main";

const history = createBrowserHistory();

class App extends React.Component {

    constructor(props) {
        super(props);
        this.store = new Store();
    }

    render() {
        return (
            <div className="App">
                <Provider store={this.store}>
                    <Router history={history}>
                        <Switch>
                            <Route path="/" component={MainPage} exact/>
                        </Switch>
                    </Router>
                </Provider>
            </div>
        );
    }
}

export default observer(App);
