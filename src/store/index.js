import {observable, action, computed} from "mobx";

class Store {
    @observable grid = [3, 4];
    @observable containerWidth = 0;

    @computed get camWidth() {
        return `${100 / this.grid[0]}%`
    }

    @computed get camHeight() {
        return (this.containerWidth / this.grid[0])
    }

    @action
    setGrid(grid) {
        this.grid = grid;
    }

    @action
    setContainer(width) {
        this.containerWidth = width;
    }
}

export default Store;